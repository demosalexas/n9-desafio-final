class Moradia(object):

    def __init__(self, area, endereco, numero, preco_imovel, tipo='Não especificado'):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = tipo

    def gerar_relatorio_preco(self):
        return self.preco_imovel / self.area


class Apartamento(Moradia):

    def __init__(self, area, endereco, numero, preco_imovel, preco_condominio, num_apt, num_elevadores, tipo='Apartamento' ):
        super().__init__(area, endereco, numero, preco_imovel)
        self.tipo = tipo
        self.preco_condominio = preco_condominio
        self.num_apt = num_apt
        self.num_elevadores = num_elevadores
        self.andar = int(str(num_apt)[0:-1])

    def gerar_relatorio(self):

        return self.endereco + ' - ' + str(self.num_apt) + ' - ' + 'andar: ' + str(self.andar) + ' - elevadores: ' + \
               str(self.num_elevadores) + ' - preço por m²: R$ ' + str(self.gerar_relatorio_preco())


